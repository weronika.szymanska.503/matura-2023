plik = open("losowe-numery.txt")
losowe_liczby = plik.read()
# w tym miejscu już czytasz wartość pliku i przypisujesz ją do zmiennej, więc nie musisz tego robić drugi raz
# jeżeli plik wygląda w ten sposób, że masz linijkę po linijce, to warto wykorzystać medotę 'readlines'
# ponieważ wtedy wynik od razu dzieli Ci się na elegancką listę
# losowe_liczby = plik.readlines()
# losowe_liczby -> ['14\n', '41\n', '5\n', '84\n', '73\n', '39\n', '85\n', '36\n', '46\n', '39\n']
# ale Twoje rozwiązanie też jest w porządku :)

print(losowe_liczby)

linia = losowe_liczby

# tab = linia.split(" ")
# w tym przypadku jak dzielisz na podstawie pustej przestrzeni, wychodzi Ci coś takiego:
# tab -> ['14\n41\n5\n84\n73\n39\n85\n36\n46\n39\n']
# ponieważ linie nie są rozdzielone pustą przestrzenią, tylko znakiem nowej linii '\n', co widać, jak spróbujesz sobie
# wyprintować to, co aktualnie przechowuje zmienna `tab`, znak nowej linii jest niewidoczny na konsoli
# dlatego też `.split()` nie robi Ci listy kilku elementowej, tylko skleja to do jednego stringa w liście jednoelementowej,
# bo nie znajduje tych pustych pól, później występuje ValueError przez to, bo się wartości nie zgadzają

tab = linia.split("\n")
# tab -> ['14', '41', '5', '84', '73', '39', '85', '36', '46', '39', '']
# w pliku generuje się ostatnia linijka, która jest pusta, dlatego trzeba ją usunąć, nie można pustego elementu zamienić
# na liczbę

tab.pop()
# tab -> ['14', '41', '5', '84', '73', '39', '85', '36', '46', '39']

tab = [int(liczba) for liczba in tab]
# tab -> [14, 41, 5, 84, 73, 39, 85, 36, 46, 39] # teraz wszystko jest liczbą, a nie stringiem

suma = 0

# for liczba in losowe_liczby:
#     suma = suma + liczba
# tutaj będzie TypeError, ponieważ próbujesz dodać stringi do liczb - `losowe_liczby` to jest to, co odczytałaś z pliku,
# a Ty chcesz dodać liczby z `tab`, prawda? :)

for liczba in tab:
    suma = suma + liczba

print(f"Suma liczb to {suma}")
