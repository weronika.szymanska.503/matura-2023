plik = open("zen.txt")
zen = plik.readlines()
# czytamy plik linijka po linijce, żeby otrzymać już listę podzieloną na elementy, gdzie jeden element to jedna
# linijka tekstu
# zen -> ['Beautiful is better than ugly.\n', 'Explicit is better than implicit.\n', ...]

# skoro mamy tekst podzielony już na linie i przechowujemy to w zmiennej typu lista
# to teraz tylko wystarczy zamienić kolejność elementów w liście
# można to zrobić za pomocą metody 'reverse()', która odwraca listę na "drugą stronę"

zen.reverse()
# zen -> ["Namespaces are one honking great idea -- let's do more of those!", 'If the implementation is easy to explain\n, ...]

# pierwszy element `zen` nie ma znaku nowej linii, więc trzeba go dodać, żeby potem móc ładnie wydrukować tekst na konsoli
zen[0] = zen[0] + "\n"
# zen -> ["Namespaces are one honking great idea -- let's do more of those!\n", 'If the implementation is easy to explain\n, ...]

for linijka in zen:
    print(linijka)
