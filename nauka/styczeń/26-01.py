
slownik = {'A':'Ą','Ą':'B','B':'C','C':'Ć','Ć':'D','D':'E','E':'Ę','Ę':'F','F':'G','G':'H','H':'I','I':'J','J':'K','K':'L','L':'Ł','Ł':'M','M':'N','N':'Ń','Ń':'O','O':'Ó','Ó':'P','P':'Q','Q':'R','R':'S','S':'Ś','Ś':'T','T':'U','U':'W','W':'X','X':'Y','Y':'Z','Z':'Ź','Ź':'Ż','Ż':'A'}

n = input("wprowadz zdanie: ")

duze_litery = n.upper().replace(' ','')
print(duze_litery)

with open('odp_26.txt', 'w') as plik:
    if '!' in duze_litery:
        zdanie = duze_litery.strip('!')
        print(zdanie)
        for litera in zdanie:
                plik.write(f'{slownik[litera]}')
    else:
        for litera in duze_litery:
                plik.write(f'{slownik[litera]}')

