# DOPISZ - dodanie ostatniej litery
#
# gdzieś będzie pętla
#
# DOPISZ -> lista.append()
# ZMIEN -> lista[-1] = nowa_wartosc
# USUN -> lista.pop()
# PRZESUN -> lista[i] = przesunieta_wartosc

alfabet = ["A", "B", "C", "D", "E", "F"]
alfabet_po_przesunieciu = ["B", "C", "D", "E", "D", "G"]
ilość_wystąpień = [1, 3]

alfabet_slownik = {"A":"B", "B": "C", "C": "D", "D":"E", "E": "F", "F":"G", "G": "H",
                   "H":"I", "I":"J", "J":"K", "K":"L", "L":"M", "M": "N",
                   "N":"O", "O":"P", "P":"R", "S":"T", "T":"U", "U":"W",
                   "W":"X", "X":"Y", "Y":"Z", "Z":"A"}

wynik = []

plik = open("instrukcje.txt")
instrukcje = plik.readlines()
#instrukcje.strip()

instrukcje2 = []

for znak in instrukcje:
    znak.strip("\n")
    instrukcje2.append(znak)

#print(instrukcje)
#print(instrukcje2)

for instrukcja in instrukcje:
    krok = instrukcja.split()
    if krok[0] == "DOPISZ":
        wynik.append(krok[1])
    if krok[0] == "ZMIEN":
        wynik[-1] = krok[1]
    if krok[0] == "USUN":
        wynik.pop()
    if krok[0] == "PRZESUN":
        print(wynik)
        indeks = wynik.index(krok[1])
        wynik[indeks] = alfabet_slownik[wynik[indeks]]

# print(instrukcje[0])
# wynik.append(instrukcje[0][-2])
#
# print(wynik)
#
# print(instrukcje[1])
# wynik[0] = alfabet_slownik[wynik[0]]
#
print(wynik)