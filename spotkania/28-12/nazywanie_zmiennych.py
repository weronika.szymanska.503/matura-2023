from datetime import datetime

# nazwa kursu
nazwa_kursu = "Python"

# opis kursu
opis_kursu = "Język w mowie węży."

# trudność kursu, poziom określony za pomocą liczby typu int w przedziale 1 - 10
poziom_kursu = 7

# liczba modułów w kursie
moduly_kursu = 15

# liczba testów w kursie
testy_kursu = 20

# liczba projektów w kursie
liczba_projektow = 5

aktualna_data_czas = "12:30 12-09-2023"

# zmienna przechowująca wygenerowaną datę wraz z godziną
wygenerowana_data_czas = datetime.strptime('04/27/95 07:14:22', '%m/%d/%y %H:%M:%S')

# nazwa użytkownika + jego numer ID
nazwa = 'Bob'  #  uzytkownik / nazwa_uzytkownika
numer_id = 1621535852  # nr_id

# lista z numerami ostatniego losowania lotto
lista = [1, 2, 3, 4]  # lottery_list / wyniki_lista

# liczba użytkowników, którzy wzięli udział w losowaniu
ilosc_uzytkownikow_losowania = 42

# wyświetlanie rekomendowanych krajów
rekomendowane_kraje = ["UK", "USA", "UAE"]

for kraj in rekomendowane_kraje:
    print(kraj)

# funkcje do pobierania różnych danych


def get_users():  # weź_uzytkownika
    # do something
    pass


def get_user(id):  # weź_uzytkownika(id)
    # do something
    pass


def get_posts():  # weź_posty
    # do something
    pass


def get_post(id):  # weź_posty(id)
    # do something
    pass
