plik = open("binarne.txt")

binarne = plik.readlines()

lista_liczb = []
for linijka in binarne:
    lista_liczb.append(linijka.strip("\n"))

print(lista_liczb)

niepoprawny = 0
dlugosc_najkrotszej_niepoprawnej_liczby = 0

for liczba in lista_liczb:
    segmenty = [liczba[i: i + 4] for i in range(0, len(liczba), 4)]
    print(liczba)
    print(segmenty)
    for seg in segmenty:
        wartosc_segmentu = (int(seg[0]) * 8) + (int(seg[1]) * 4) + (int(seg[2]) * 2) + (int(seg[3]) * 1)
        print(wartosc_segmentu)
        if wartosc_segmentu > 9:
            niepoprawny += 1
            print(f"liczba {liczba} jest niepoprawna")
            if dlugosc_najkrotszej_niepoprawnej_liczby == 0:
                dlugosc_najkrotszej_niepoprawnej_liczby = len(liczba)
            else:
                if dlugosc_najkrotszej_niepoprawnej_liczby > len(liczba):
                    dlugosc_najkrotszej_niepoprawnej_liczby = len(liczba)
            break

print(f"ilość niepoprawnych: {niepoprawny}")
print(f"dlugosc najkrotrzego napisu: {dlugosc_najkrotszej_niepoprawnej_liczby}")
