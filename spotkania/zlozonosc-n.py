# def print_first_element(lst):
#     if lst:
#         print(lst[0]) # złożoność stałą 0(1)
#
# print_first_element("abcdefg")
#
# zmienna = "sdfslwler"
# lista = ["1", "2"]
# print_first_element(zmienna)
# print(print_first_element(lista))
#
#
# pierwsza_litera = input("podaj stringa")
# print(str(pierwsza_litera[0])) #  złożoność stałą = O(1)

def wypisz(n):
    print("nazwa nazwa")

def nazwa_funkcji(jakie_argumenty_przyjmuje_funkcja):
    # wpisujesz co funkcja robi
    print(jakie_argumenty_przyjmuje_funkcja)

nazwa_funkcji("hello world")
wypisz(10)
wypisz(100000000)  # złożoność stała


def wypisz_tyle_razy(n):
    for n in range(n):
        print("nazwa")
        print("kolejny string")
        print(str(n))


wypisz_tyle_razy(5)
wypisz_tyle_razy(10)
